# --------------------( LICENSE                           )--------------------
# Copyright 2014-2022 by Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# CAUTION: This previously working Appveyor configuration no longer works and
# has thus been disabled. It seems likely that this line below is the culprit:
#     - cmd: set "PATH=%MINICONDA_DIRNAME%;%MINICONDA_DIRNAME%\\Scripts;%PATH%;"
# That is *NOT* the official way to enable Anaconda support within Appveyor.
# Nonetheless, Microsoft Windows is currently a sufficiently low priority to
# render this ignorable. Science == POSIX. Microsoft Windows users would be
# better served by their installing the Windows Subsystem for Linux (WSL) than
# by our throwing scarce volunteer resources at a dissipating audience.
#
# To reenable Appveyor support at some future date:
# * Visit the "Settings / General" configuration panel on GitLab.
# * Add a new badge with these settings:
#   https://ci.appveyor.com/api/projects/status/{HIDDEN}/branch/%{default_branch}?svg=true
#        ...where "{HIDDEN}" is a secret string known only to us.
# * Visit the "Settings / Webhooks" configuration panel on GitLab.
# * Add a new webhook with these settings:
#   URL: https://ci.appveyor.com/api/gitlab/webhook?id={HIDDEN}
#        ...where "{HIDDEN}" is a secret string known only to us.
#   Trigger: "Push events" and "Tag push events"
#   SSL: Enable SSL verification
#
# That said, it's probably simpler and saner to simply migrate from GitLab to
# GitHub at this point. The former offers:
# * Unlimited CI minutes through GitHub Actions.
# * Transparent cross-platform CI support for *ALL* supported platforms
#   (including Microsoft Windows) through GitHub Actions.
# * GitHub Sponsors enabling a prospective funding drive.
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# --------------------( SYNOPSIS                          )--------------------
# Project-wide Appveyor configuration, integrating the third-party
# Windows-specific free-as-in-beer continuous integration (CI) service exposed
# by Appveyor with this project's "py.test"-driven test suite.
#
# Due to the non-trivial complexity in computational space, computational time,
# and developer time and sanity of installing scientific Python packages
# (particularly packages requiring C extensions) via "pip", this configuration
# leverages the Miniconda distribution instead. Doing so dramatically reduces
# the aforementioned complexity with a minor increase in redundancy, requiring
# the mandatory dependencies already declared by this project's top-level
# "setup.py" script as PyPI-specific packages be redeclared as
# Miniconda-specific packages. While unctuous, the otherwise significant
# reduction in complexity will brook no argument.
#
# --------------------( SEE ALSO                          )--------------------
# * https://www.appveyor.com/docs/installed-software/#python
#   *THIS IS ABSOLUTELY ESSENTIAL,* providing the canonical list of all Python-
#   oriented software unconditionally installed into all CI environments
#   complete with the absolute paths, word sizes, and versions of such
#   software. The configuration below is critically dependent upon this list.
# * http://tjelvarolsson.com/blog/how-to-continuously-test-your-python-code-on-windows-using-appveyor/
#   Relatively recent, reasonably well-written blog post articulating the
#   simplest Appveyor configuration establishing a sane Miniconda testing
#   environment *WITHOUT* the usual obsolete and hence insane edge-case
#   handling typically littering these configurations.
# * https://github.com/conda-forge/staged-recipes/blob/master/appveyor.yml
#   Real-world "appveyor.yml" configuration manually establishing a sane
#   Miniconda testing environment *WITHOUT* leveraging external automation
#   (e.g., the "appveyor/install-miniconda.ps1" script). This configuration
#   arguably remains the de-facto and most popular solution for doing so.
# * https://raw.githubusercontent.com/astropy/astropy/master/appveyor.yml
#   Real-world "appveyor.yml" configuration used by a prominent scientific
#   Python framework and largely considered to be the canonical scientific
#   Appveyor configuration as of this writing. The "appveyor.yml" files of
#   multiple other projects (e.g., SpiceyPy) reference this "appveyor.yml"
#   file, which does indeed appear to be subjectively authoritative.
# * https://github.com/astropy/ci-helpers
#   Utility scripts run by the above configuration to establish a sane
#   Miniconda testing environment, notably the "appveyor/install-miniconda.ps1"
#   script. These scripts are sufficiently general-purpose and well-documented
#   as to be cloned and run by an assortment of third-party Appveyor
#   configuration files unaffiliated with the AstroPy project. Doing so
#   presents numerous concerns, however, including:
#   * Efficiency, as:
#     * Each Appveyor run would repetitively clone this repository.
#     * "install-miniconda.ps1" is technically specific to the AstroPy project
#       and thus contains logic of interest *ONLY* to that project.
#     * "install-miniconda.ps1" predates Appveyor's useful decision to
#       unconditionally install Miniconda into all CI environments and has yet
#       to be refactored accordingly. Because of this, that script expends
#       non-trivial space and time uselessly re-downloading and re-installing
#       Miniconda into the current CI environment.
#   * Maintainability, as that repository is under third-party control.
#   This Appveyor configuration has opted to manually establish a sane
#   Miniconda testing environment without leveraging this repository,
#   preserving both efficiency and maintainability.
# * https://packaging.python.org/appveyor
#   Quasi-official documentation providing the canonical "appveyor.yml"
#   skeleton configuration for use in existing Python projects. Sadly, this
#   skeleton leverages the general-purpose "pip" package manager rather than
#   the Anaconda-specific "conda" package manager and hence is sadly useless.

# ....................{ TODO                               }....................
#FIXME: Auto-generate a pip-compliant binary Windows wheel file for this
#project *AFTER* all successful test runs. For examples, see:
#    https://raw.githubusercontent.com/AndrewAnnex/SpiceyPy/master/appveyor.yml

# ....................{ CONFIGURATION                      }....................
# Disable .NET integration. Python projects are implicitly built during
# installation.
build: false


matrix:
  # Enable the so-called "fast fail strategy," halting the entire test process
  # on the first profile failure.
  fast_finish: true

# ....................{ CONFIGURATION ~ matrix            }....................
platform:
  # Exercise only 64-bit binaries.
  - x64


environment:
  # Dictionary mapping from the name to the value of each global environment
  # variable to unconditionally declare for *ALL* build profiles defined by the
  # "matrix" list below.
  # global:
    # Whitespace-delimited list of the names of all Miniconda channels required
    # by third-party Miniconda packages listed in "CONDA_DEPENDENCIES".
    # CONDA_CHANNELS: ""

    # Whitespace-delimited list of the names of all PyPI-packaged packages to
    # be installed with "pip" required by this project and *NOT* installable
    # with Miniconda. This is a fallback that should be used only where needed.
    # PIP_PACKAGE_NAMES: ""

  # List of all build profiles, each of which is a dictionary mapping from the
  # name to the value of each global environment variable to be conditionally
  # declared for this profile.
  #
  # This matrix exercises all supported major 64-bit Python 3.x versions,
  # ignoring changes between minor versions, all Python 2.x versions, *AND* all
  # 32-bit Python versions. While this project technically supports the same
  # 32-bit Python 3.x versions, 32-bit architectures are explicitly unsupported
  # (e.g., due to the 4GB memory barrier) and hence ignorable. See also:
  #
  # * The official list of all supported Miniconda environments under Appveyor:
  #   https://www.appveyor.com/docs/build-environment/#miniconda
  # * The "betse.metadata.PYTHON_VERSION_MIN" string global, defining the
  #   minimum version of Python 3.x supported by this project. All stable
  #   versions of Python 3.x greater than or equal to this minimum version
  #   *MUST* be explicitly listed below.
  # * The "betse.metadeps.RUNTIME_MANDATORY" tuple global, defining the minimum
  #   version of Numpy supported by this project.
  matrix:
    #FIXME: Uncomment after AppVeyor bumps Miniconda support for Python 3.8.
    #- PYTHON_VERSION: 3.9
    #  MINICONDA_DIRNAME: C:\Miniconda39-x64
    - PYTHON_VERSION: 3.8
      MINICONDA_DIRNAME: C:\Miniconda38-x64
    - PYTHON_VERSION: 3.7
      MINICONDA_DIRNAME: C:\Miniconda37-x64
    - PYTHON_VERSION: 3.6
      MINICONDA_DIRNAME: C:\Miniconda36-x64

# ....................{ COMMANDS                          }....................
# List of all external commands to be run for each profile *BEFORE* those
# listed within "install" below.
init:
  # For debuggability, visually prefix each profile by (in order):
  #
  # * The absolute path of the active Python interpreter.
  # * The version of this interpreter.
  # * The word size of this interpreter.
  # * The absolute path of Miniconda.
  - ECHO %PYTHON_VERSION% %MINICONDA_DIRNAME%


# List of all external commands to be run for each profile *BEFORE* those
# listed within "test_script" below.
install:
  #FIXME: The following line, although supposedly the officially supported
  #solution, does *NOT* behave as expected and has thus been reverted back to
  #the manual approach of manually munging the %PATH% environment variable.
  # - cmd: CALL "%MINICONDA_DIRNAME%\\Scripts\\activate.bat"

  # Prepend the current %PATH% by the absolute paths of all Miniconda-managed
  # directories containing the active Python interpreter and standard Python
  # scripts (e.g., "easy_install") installed with this interpreter, ensuring
  # that the Miniconda-specific rather than system-wide version of Python is
  # run (e.g., "C:\Miniconda3-x64" rather than "C:\Python34-x64").
  #
  # Note that this *MUST* be done prior to running the "conda" command.
  - cmd: set "PATH=%MINICONDA_DIRNAME%;%MINICONDA_DIRNAME%\\Scripts;%PATH%;"

  # Configure "conda" to run headless. Dismantled, this is:
  #
  # * "always_yes true", automatically pass the "--yes" option to *ALL*
  #   "conda" commands run below. This is a superficial convenience reducing
  #   the likelihood of developer oversight and hence saving essential sanity.
  #
  # Note that the "auto_update_conda" option is unsupported by the older
  # versions of "conda" installed with Appveyor and hence omitted here.
  - cmd: conda config --set always_yes true

  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  # NOTE: See similar logic in ".gitlab-ci.yml" for further commentary. To
  # reduce DRY, these duplicated commands are only minimally commented.
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  # Add the unofficial "conda-forge" channel as the highest-priority channel.
  - cmd: conda config --add channels conda-forge

  # Update Miniconda to its most recent stable release.
  #
  # Note that this approach to updating Minicoda is regarded by the Anaconda
  # community as obsolete. Unfortunately, replacing this approach with the
  # following industry-standard approach (as our comparable ".gitlab-ci.yml"
  # configuration does) results in fatal exceptions on AppVeyor spinup:
  #     - cmd: conda install --quiet --name base conda
  - cmd: conda update --quiet conda

  # Explicitly activate the base Anaconda environment *BEFORE* running the
  # "conda info" subcommand. Why? Because failing to do so reliably raises a
  # non-human-readable exception. See also the following inscrutable issue:
  #     https://github.com/conda/conda/issues/8836
  #
  # Note that this appears to be a Windows-specific issue and is thus
  # intentionally omitted from our Linux-specific ".gitlab-ci.yml"
  # configuration.
  - cmd: activate
  # - cmd: conda activate base

  # For debuggability, print metadata identifying this Miniconda release.
  - cmd: conda info --all

  #FIXME: This environment should probably be cached. Consider researching
  #Appveyor-specific conda caching.

  # Create an empty Anaconda environment specific to this Python version.
  - cmd: conda create --quiet --name conda-env-%PYTHON_VERSION% python=%PYTHON_VERSION%

  # Activate this environment (i.e., prepend the current %PATH% by this
  # environment's top-level directory).
  - cmd: activate conda-env-%PYTHON_VERSION%
  # - cmd: conda activate conda-env-%PYTHON_VERSION%

  # Install all mandatory and optional dependencies of this application into
  # this environment from the "conda-forge" channel.
  - cmd: conda install --quiet --file requirements-conda.txt

  # Install this project into this environment in the most efficient means
  # possible (i.e., without copying this project into this environment).
  - cmd: python setup.py develop


# List of all external commands to be run for each CI pipeline *AFTER* those
# listed within "install" above.
test_script:
  # For debuggability, print metadata identifying the current version of this
  # application. See similar logic in ".gitlab-ci.yml" for details.
  - cmd: betse --headless info

  # Run the following multiline Powershell script.
  #
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  # CAUTION: PowerShell insanely interprets *ANY* attempt to write to stderr as
  # a fatal exception, resulting in output resembling the following and an
  # immediate pipeline failure:
  #
  #     At line:5 char:1
  #     + py.test --maxfail=3
  #     + ~~~~~~~~~~~~~~~~~~~
  #         + CategoryInfo          : NotSpecified: ([py.test] Windo...nstructions at::String) [], RemoteException
  #         + FullyQualifiedErrorId : NativeCommandError
  #
  # Ideally, this would be trivially circumventable by simply instructing
  # PowerShell to redirect stderr to stdout. Sadly, this appears to be
  # infeasible. Our only recourse is to avoid writing to stderr in the first
  # place from this application's codebase, requiring that this codebase:
  #
  # * Implicitly detect PowerShell at runtime.
  # * When detected, unconditionally log *ALL* messages to stdout.
  #
  # While blatantly non-ideal, there's little else that we can do to resolve
  # PowerShell idiosyncrasies.
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  - ps: |
      # Run the entire "py.test"-based test suite under similar options to
      # those set via ".gitlab-ci.yml". Dismantled, this is:
      #
      # * '| %{ "$_" }', coercing the high-level PowerShell stream object
      #   constructed by running the "py.test" command into a low-level string.
      #   If this is *NOT* done, then PowerShell insanely interprets *ANY*
      #   attempt to write to stderr as a fatal exception as detailed above.
      #   See also the following relevant StackOverflow answers:
      #   * https://stackoverflow.com/a/20950421/2809027
      #   * https://stackoverflow.com/a/12866669/2809027
      py.test --maxfail=3 2>&1 | %{ "$_" }

      #FIXME: To assist in debugging this, also print a non-fatal warning with
      #the actual exit status reported above.

      # If the exit status reported by "py.test" is non-zero but nonsensical,
      # coerce this status to zero. For unknown reasons, some hellish
      # combination of Appveyor, Python, "py.test", and our test suite causes
      # successful runs to unpredictably report nonsensical non-zero exit
      # status. To preserve developer sanity, these false negatives *MUST* be
      # explicitly ignored. Happily, the exit status reported by these false
      # negatives all reside in the same range of extremely negative integers.
      #
      # Note that Powershell sets the $LastExitCode integer global *ONLY* on
      # running external commands. If statements do *NOT* trigger this
      # behaviour. Hence, the current value of this global is preserved if this
      # if statement evaluates to False.
      if ($LastExitCode -le -1000000000) { $host.SetShouldExit(0) }
